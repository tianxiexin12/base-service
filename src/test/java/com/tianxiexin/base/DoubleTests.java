package com.tianxiexin.base;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * json转换
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DoubleTests {

    /**
     * 截取指定数位并四舍五入
     */
    @Test
    public void roundTest() {
        double bmi = 3.782;
        //
        //方案一：
        bmi = (double) Math.round(bmi * 100) / 100;
        System.out.println(bmi);
        //方案二：
        DecimalFormat df = new DecimalFormat("#.00");
        bmi = Double.parseDouble(df.format(bmi));
        System.out.println(bmi);
        //方案三：
        bmi = Double.parseDouble(String.format("%.2f", bmi));
        System.out.println(bmi);
        //方案四
        BigDecimal bg = new BigDecimal(bmi);
        double f1 = bg.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out.println("f1:" + f1);

    }


}
