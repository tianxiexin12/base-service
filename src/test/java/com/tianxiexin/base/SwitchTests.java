package com.tianxiexin.base;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * json转换
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SwitchTests {

    /**
     * switch支持byte、short、int、char;
     * 1.5开始支持枚举；
     * 1.7开始支持String。
     */
    @Test
    public void parameterTest() {
        int num = 0;
        switch (num) {
            case 0:
                System.out.println(num);
                break;
            default:
                System.out.println("未匹配");
        }

        char b = 'a';
        switch (b) {
            case 'a':
                System.out.println(b);
                break;
            default:
                System.out.println("未匹配");
        }

        short shortNum = 1;
        switch (shortNum) {
            case 1:
                System.out.println(b);
                break;
            default:
                System.out.println("未匹配");
        }

        byte newByte = 2;
        switch (newByte) {
            case 2:
                System.out.println(newByte);
                break;
            default:
                System.out.println("未匹配");
        }

    }


}
