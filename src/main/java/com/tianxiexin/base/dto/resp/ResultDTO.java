package com.tianxiexin.base.dto.resp;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultDTO<T> implements Serializable {
	public static final int ERROR_HTTP_STATUS = 500;
	private static final long serialVersionUID = 1L;
	
	private boolean ok;
	private String code;
	private String msg;
	private T result;

	//暂时增加无code构造方法，后续优化code
	public ResultDTO(boolean ok, String msg){
	    this(ok,null,msg,null);
	}
	
	//暂时增加无code构造方法，后续优化code
	public ResultDTO(boolean ok, String msg, T value){
        this(ok,null,msg,value);
    }
	
	public ResultDTO(boolean ok, String code, String msg) {
		this(ok, code, msg, null);
	}

	@Override
    public String toString() {
        return "ResultDTO [ok=" + ok + ", code=" + code + ", msg=" + msg + ", result=" + result
               + "]";
    }

	//暂时增加无code ok方法，后续优化code
	public static<T> ResultDTO<T> ok(T ret) {
        return new ResultDTO<T>(true, "", null, ret);
    }
		
    //暂时增加无code ok方法，后续优化code
	public static<T> ResultDTO<T> ok(String msg, T ret) {
        return new ResultDTO<T>(true, "", msg, ret);
    }
	
	public static<T> ResultDTO<T> ok(String code, String msg, T ret) {
		return new ResultDTO<T>(true, code, msg, ret);
	}
	
	public static<T> ResultDTO<T> error(String code, String msg) {
		return new ResultDTO<T>(false, code, msg, null);
	}

}
