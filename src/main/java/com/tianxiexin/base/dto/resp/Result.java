/**
 * sinafenqi.com
 * Copyright (c) 2018 All Rights Reserved.
 */
package com.tianxiexin.base.dto.resp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> {
    private Integer status;
    private String msg;
    private T data;
}
