package com.tianxiexin.base.dto.resp.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CacheDTO {
	private String key;
	private String value;
	private int seconds;

	public CacheDTO(String key) {
		this.key = key;
	}
	
	public CacheDTO(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public static CacheDTO me(String key) {
		return new CacheDTO(key);
	}
	
	public static CacheDTO me(String key, int seconds) {
		return new CacheDTO(key, null, seconds);
	}
	
	public static CacheDTO me(String key, String value) {
		return new CacheDTO(key, value);
	}
	
	public static CacheDTO me(String key, String value, int seconds) {
		return new CacheDTO(key, value, seconds);
	}
	
	@Override
	public String toString() {
		return "CacheDTO [key=" + key + ", value=" + value + ", seconds=" + seconds + "]";
	}
}
