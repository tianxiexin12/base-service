package com.tianxiexin.base.feign;

import com.tianxiexin.base.dto.resp.ResultDTO;
import com.tianxiexin.base.dto.resp.cache.CacheDTO;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 缓存调用类
 * @author wukang
 *  注意：Feign不支持PostMapping、GetMapping这种形式
 */
@FeignClient(name = "cache-service")
public interface CacheRemoteService {

    /**
     * 设置缓存
     * @param dto: key:缓存Key
     *             value:缓存值
     */
    @RequestMapping(value = "/caches/set",method = RequestMethod.POST)
    ResultDTO<String> set(CacheDTO dto);
}
